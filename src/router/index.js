import Vue from 'vue'
import Router from 'vue-router'
import Wizard from '../components/Wizard'
import Dashboard from '../components/Dashboard'
import NotFound from '../components/error/NotFound'
import WhatIs from '../components/libranzas/WhatIs'
import Advantage from '../components/libranzas/Advantage'
import Term from 'components/TermsConditions'
import Login from 'components/login'
import Requests from 'components/Requests'
import store from '../store/index'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      name: 'flow',
      path: '/flow',
      component: Wizard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/',
      component: Dashboard,
      children: [
        {
          path: '/',
          component: WhatIs
        },
        {
          path: '/Advantage',
          component: Advantage
        },
        {
          path: '/WhatIs',
          component: WhatIs
        }
      ]
    },
    {
      path: '/404',
      component: NotFound
    },
    {
      name: 'termsConditions',
      path: '/terms',
      component: Term,
      meta: {
        requiresAuth: true
      }
    },
    {
      name: 'login',
      path: '/login',
      component: Login,
    },
    {
      name: 'requests',
      path: '/requests',
      component: Requests,
    },
    {
      name: 'notFound',
      path: '*',
      redirect: '/404'
    }]
})

router.beforeEach((to, from, next) => {
  let user = store.state.authUser
  if (to.meta.requiresAuth) {
    if (!user) {
      next({name: 'login'})
    } else {
      if (to.name == 'flow' && !store.state.authTerms)
        next({name: 'termsConditions'})
      else
        next()
    }
  } else {
    if (to.name == 'login' && user)
      next({name: from.name})
    else
      next()
  }
})

export default router
