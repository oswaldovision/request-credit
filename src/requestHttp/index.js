import axios from 'axios'
// import store from '../store'

axios.defaults.baseURL = 'http://localhost:3045/v1'

const requestHttp = {
  get: (url, params, callback) => {
    axios.get(url, params)
      .then(res => {
        callback(null, res.data)
      })
      .catch(err => {
        callback(err)
      })
  },
  post: (url, params, callback) => {
    axios.post(url, params)
      .then(res => {
        callback(null, res)
      })
      .catch(err => {
        callback(err)
      })
  },
  delete: (url, params, callback) => {
    axios.delete(url, params)
      .then(res => {
        callback(null, res)
      })
      .catch(err => {
        callback(err)
      })
  }
}

export default requestHttp
