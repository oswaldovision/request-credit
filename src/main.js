import Vue from 'vue'
import router from 'router'
import App from './components/App.vue'
import store from './store'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faAddressBook } from '@fortawesome/free-regular-svg-icons'
import { faGoogle, faFacebook, faTwitter, faMicrosoft } from '@fortawesome/free-brands-svg-icons'
import { faCoffee, faSignOutAlt, faSignInAlt, faPiggyBank } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faCoffee, faGoogle, faFacebook, faTwitter, faMicrosoft, faAddressBook, faSignOutAlt, faSignInAlt, faPiggyBank)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

var vm = new Vue({
  el: '#app',
  router,
  render: h => h(App),
  store
})
