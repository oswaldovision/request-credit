import { required } from 'vuelidate/lib/validators'

let validatorModel = {
  validateAccount: {
    required
  },
  typeAccount: {
    required
  },
  numAccount: {
    required
  },
  bank: {
    required
  },
  form: [
    'validateAccount',
    'typeAccount',
    'numAccount',
    'bank'
  ]
}

export default validatorModel
