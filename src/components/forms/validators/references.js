import { required, minLength } from 'vuelidate/lib/validators'

let validatorModel = {
  personalReferences: {
    required,
    minLength: minLength(2),
    $each: {
      name: {required},
      lastName: {required},
      address: {required},
      city: {required},
      celPhone: {required},
      jobPhone: {required}
    }
  },
  familiarReferences: {
    required,
    minLength: minLength(2),
    $each: {
      name: {required},
      lastName: {required},
      address: {required},
      city: {required},
      celPhone: {required},
      jobPhone: {required}
    }
  },
  form: [
    'personalReferences',
    'familiarReferences'
  ]
}

export default validatorModel
