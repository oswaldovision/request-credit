import { required, minValue } from 'vuelidate/lib/validators'

let validatorModel = {
  amount: {
    required,
    minValue: minValue(5000000)
  },
  term: {
    required,
    minValue: minValue(12)
  },
  form: ['amount', 'term']
}

export default validatorModel
