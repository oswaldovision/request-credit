import { required, minValue } from 'vuelidate/lib/validators'

let validatorModel = {
  businessSocial: {
    required
  },
  nit: {
    required
  },
  addressCompany: {
    required
  },
  phoneCompany: {
    required
  },
  fax: {},
  boss: {
    required
  },
  dateInit: {
    required
  },
  position: {
    required
  },
  salary: {
    required
  },
  otherIncomes: {},
  totalIncomes: {},
  familiarExpenses: {
    required
  },
  form: [
    'businessSocial',
    'nit',
    'addressCompany',
    'phoneCompany',
    'fax',
    'boss',
    'dateInit',
    'position',
    'salary',
    'otherIncomes',
    'familiarExpenses',
    'totalIncomes'
  ]
}

export default validatorModel
