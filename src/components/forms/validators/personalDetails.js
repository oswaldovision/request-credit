import { required, email } from 'vuelidate/lib/validators'

let validatorModel = {
  name: {
    required
  },
  lastName: {
    required
  },
  secondLastName: {},
  genre: {
    required
  },
  civilStatus: {
    required
  },
  dateBirth: {
    required
  },
  placeBirth: {
    required
  },
  typeDoc: {
    required
  },
  numDoc: {
    required
  },
  expeditionPlace: {
    required
  },
  numChildren: {
    required
  },
  typeHousing: {
    required
  },
  celPhone: {
    required
  },
  address: {
    required
  },
  neighborhood: {
    required
  },
  city: {
    required
  },
  stateCity: {
    required
  },
  email: {
    required,
    email
  },
  phone: {},
  form: [
    'name',
    'lastName',
    'secondLastName',
    'genre',
    'civilStatus',
    'dateBirth',
    'placeBirth',
    'typeDoc',
    'numDoc',
    'expeditionPlace',
    'numChildren',
    'typeHousing',
    'celPhone',
    'address',
    'neighborhood',
    'city',
    'stateCity',
    'email',
    'phone'
  ]
}

export default validatorModel
