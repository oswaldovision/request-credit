import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyDIf-nwg97uBCl8H_EMFz7kRe40j8ITS8Y',
  authDomain: 'libranzas-auth.firebaseapp.com',
  databaseURL: 'https://libranzas-auth.firebaseio.com',
  projectId: 'libranzas-auth',
  storageBucket: 'libranzas-auth.appspot.com',
  messagingSenderId: '1029544021180'
}

firebase.initializeApp(config)

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    count: 0,
    authTerms: false,
    firebase : firebase,
    selectedBank: {
      index: 0,
      nameBank: '',
      img: ''
    },
    authUser: null,
    user: {
      email: '',
      typeDoc: '',
      numDoc: '',
      token: '',
      login: false
    }
  },
  getters: {
    tripleCounter: (state) => state.count * 3
  },
  mutations: {
    userRegister (state, log) {
      state.user.email = log.email
      state.user.typeDoc = log.typeDoc
      state.user.numDoc = log.numDoc
      state.user.token = log.token
      state.user.login = log.login
    },
    setAuthUser(state, user){
      state.authUser = user
    },
    increment (state) {
      state.count++
    },
    decrement (state) {
      state.count--
    },
    resetCounter (state) {
      state.count = 0
    },
    toogleAuthTerm (state) {
      state.authTerms = !state.authTerms
    },
    logOff(){
      firebase.auth().signOut()
        .then(() => {
          this.authUser = null
        })
        .catch(error => {
          alert(`🤕 ${error.message}`)
        })
    }
  }
})

export default store
